/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author 2ndyrGroupA
 */
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Exercises {

    public static ArrayList<Integer> sortArrayList(ArrayList<Integer> input) {
        Collections.sort(input);
        return input;
    }

    public static ArrayList<Integer> showRandom(ArrayList<Integer> input) {
        Collections.shuffle(input);
        return input;
    }

    public static ArrayList<Integer> moveMinimum(ArrayList<Integer> input) {
        ArrayList<Integer>num1=new ArrayList<Integer>();
        int minValue=Collections.min(input);
        num1.add(minValue);
        input.remove(input.indexOf(minValue));
        num1.addAll(input);
        return num1;
    }       

    public static Set<Integer> getEqualValues(Set<Integer> num1, Set<Integer> num2) {
        Set<Integer> num3 = new HashSet<Integer>();
        for (int x : num1) {
            for (int y : num2) {
                if (x == y) {
                    num3.add(x);
                }
            }
        }
        return num3;
    }

    public static Set<Integer> getUniqueValues(Set<Integer> num1, Set<Integer> num2) {
        Set<Integer> num3 = new HashSet<Integer>();
        Set<Integer> num4 = new HashSet<Integer>();
        num3.addAll(num1);
        num4.addAll(num2);
        num3.removeAll(num2);
        num4.removeAll(num1);
        num3.addAll(num4);
        return num3;
    }

    public static int countConcordia(Map<String, String> input) {
        int count = 0;
        for (Map.Entry<String, String> pair : input.entrySet()) {
            if (pair.getKey().substring(0, 9).contains("concordia")) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(100);
        array.add(5000);
        array.add(4563);
        array.add(290);
        array.add(434);
        array.add(65);
        array.add(90);
        array.add(35);
        array.add(64);
        //number1
        System.out.println(sortArrayList(array));
        //number2
        System.out.println(showRandom(array));
        
        //number3
        System.out.println(moveMinimum(array));
        /*Number4
            Why should we opt for isEmpty() over size?
         */
 /*number5
        
         */
        //Number6
        Set<Integer> num = new HashSet<Integer>();
        Set<Integer> num2 = new HashSet<Integer>();
        num.add(20);
        num.add(22);
        num.add(23);
        num.add(21);
        num.add(1);
        num2.add(2);
        num2.add(1);
        num2.add(25);
        num2.add(20);

        System.out.println(getEqualValues(num, num2));

        //Number7
        Set<Integer> number1 = new HashSet<Integer>();
        Set<Integer> number2 = new HashSet<Integer>();
        number1.add(20);
        number1.add(22);
        number1.add(23);
        number1.add(21);
        number1.add(1);
        
        number2.add(2);
        number2.add(1);
        number2.add(25);
        number2.add(20);
        System.out.println(getUniqueValues(number1, number2));

        //Number8
        Map<String, String> names = new HashMap<String, String>();
        names.put("concordia", "hello");
        names.put("concordian", "hi");
        names.put("aoncordian", "hi");
        names.put("concordia1", "Sweet");
        System.out.println(countConcordia(names));
    }
}
